﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;


public class GameBoard : MonoBehaviour {

	public List<GameElement> gameElements;

	public List<GameElement> poolGameElements;

	public TweenAlpha playMenuTweenAlpha;
	public TweenAlpha winMenuTweenAlpha;

	public List<UISprite> bgSprites;

	public int gridSize;

	private int elementsize;

	public void StartGame()
	{
		playMenuTweenAlpha.PlayForward ();
		Init ();
	}

	void Init(){

		elementsize = 1000 / gridSize;

		for (int i = 0; i < poolGameElements.Count; i++) 
		{
			if (i < gridSize * gridSize) 
			{
				poolGameElements [i].gameObject.SetActive (true);

				bgSprites [i].gameObject.SetActive (true);
				bgSprites [i].transform.localPosition = TakeElementPositionFromNumber(i);
				bgSprites [i].width = 900 / gridSize;
				bgSprites [i].height = 900 / gridSize;

				gameElements.Add (poolGameElements [i]);
				gameElements [i].Init (i,900 / gridSize);
				gameElements [i].transform.localPosition = TakeElementPositionFromNumber(i);
			}
			else
			{
				poolGameElements [i].gameObject.SetActive (false);
				bgSprites [i].gameObject.SetActive (false);
			}
		}
		Invoke ("Shuffle", 1f);
	}

	public void Shuffle(){

		System.Random rnd = new System.Random();


		do {
			for (int i = 0; i < gameElements.Count; i++) 
			{
				GameElement tmp = gameElements [i];
				gameElements.RemoveAt (i);
				gameElements.Insert (rnd.Next (gameElements.Count), tmp);
			}
		} while (!Solvable ());

		for (int i = 0; i < gameElements.Count; i++) 
		{
			gameElements [i].positionnumber = i;
			iTween.MoveTo(gameElements [i].gameObject, iTween.Hash("position", TakeElementPositionFromNumber (i ), "time", 0.2f, "easetype", iTween.EaseType.linear,"islocal",true));
		}
	}

	public void SwapGameElementsWithMove(int n, int m){
		iTween.MoveTo(gameElements [n].gameObject, iTween.Hash("position", TakeElementPositionFromNumber (m), "time", 0.1f, "easetype", iTween.EaseType.linear,"islocal",true));
		iTween.MoveTo(gameElements [m].gameObject, iTween.Hash("position", TakeElementPositionFromNumber (n), "time", 0.1f, "easetype", iTween.EaseType.linear,"islocal",true));
		SwapGameElements (n, m);

		if (CheckForWin ())
			winMenuTweenAlpha.PlayForward ();
	}

	public void SwapGameElements(int n, int m)
	{
		GameElement tmp = gameElements [n];
		gameElements [n] = gameElements [m];
		gameElements [m] = tmp;

		gameElements [n].positionnumber = n;
		gameElements [m].positionnumber = m;
	}

	public void WinGame()
	{
		Application.LoadLevel (1);
	}

	private Vector3 TakeElementPositionFromNumber(int number){
		return new Vector3 (-elementsize*(gridSize-1)/2f + (number % gridSize) * elementsize, elementsize*(gridSize-1)/2f - (number / gridSize) * elementsize,0);
	}

	private bool Solvable()
	{
		int invCount = getInvCount();

		if ((gridSize & 1) != 0)
		{
			return !Convert.ToBoolean(invCount & 1);
		}

		else 
		{
			int pos = findHolePosition();

			if ((pos & 1) != 0)
			{
				return !Convert.ToBoolean(invCount & 1);
			}
			else
			{
				return Convert.ToBoolean(invCount & 1);
			}
		}
	}

	private int getInvCount()
	{
		int inv_count = 0;
		for (int i = 0; i <gridSize * gridSize - 1; i++)
		{
			for (int j = i + 1; j < gridSize* gridSize; j++)
			{
				if (gameElements[j].number != 0 && gameElements[i].number != 0 && gameElements[i].number > gameElements[j].number)
				{
					inv_count++;
				}
			}
		}
		return inv_count;
	}

	private int findHolePosition()
	{
		for (int i = gridSize - 1; i >= 0; i--)
		{
			for (int j = gridSize- 1; j >= 0; j--)
			{
				if (gameElements[i* gridSize + j].number == 0)
				{
					return gridSize - i;
				}
			}
		}

		return 0;
	}

	private bool CheckForWin(){
		for (int i = gameElements.Count-2; i >= 0; i--) {
			if (i != gameElements [i].number-1)
				return false;
		}
		return true;
	}
}