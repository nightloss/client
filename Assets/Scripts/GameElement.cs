﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameElement : MonoBehaviour {

	public GameBoard gameBoard;

	public int number;

	public int positionnumber;

	public UILabel numberLbl;

	public UISprite sprite;

	public TweenColor tweenColor;

	public void Init(int n, int size){
		number = n+1;
		if (number != gameBoard.gridSize * gameBoard.gridSize) {
			numberLbl.text = "" + (number);
			tweenColor.PlayForward ();
			sprite.width = size;
			sprite.height = size;
		} else {
			number = 0;
			sprite.enabled = false;
		}
	}

	public void Move(){
		if (checkForMove (positionnumber % gameBoard.gridSize+1,positionnumber / gameBoard.gridSize))
			gameBoard.SwapGameElementsWithMove (positionnumber + 1, positionnumber);
		else if (checkForMove (positionnumber % gameBoard.gridSize-1,positionnumber / gameBoard.gridSize))
			gameBoard.SwapGameElementsWithMove (positionnumber - 1, positionnumber);
		else if (checkForMove (positionnumber % gameBoard.gridSize,positionnumber / gameBoard.gridSize+1))
			gameBoard.SwapGameElementsWithMove (positionnumber + gameBoard.gridSize, positionnumber);
		else if (checkForMove (positionnumber % gameBoard.gridSize,positionnumber / gameBoard.gridSize-1))
			gameBoard.SwapGameElementsWithMove (positionnumber - gameBoard.gridSize, positionnumber);
	}

	bool checkForMove(int x,int y){
		if (x >= 0 && x < gameBoard.gridSize && y >= 0 && y < gameBoard.gridSize)
		if (gameBoard.gameElements [x + y * gameBoard.gridSize].number == 0)
			return true;
		return false;
	}
}
